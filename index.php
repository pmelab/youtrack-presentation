<?php
$path = pathinfo($_SERVER['SCRIPT_NAME']);
$path = $path['dirname'] . '/';
?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">

  <title>YouTrack - The art of bugtracking</title>

  <meta name="description"
        content="A framework for easily creating beautiful presentations using HTML">
  <meta name="author" content="Hakim El Hattab">

  <meta name="apple-mobile-web-app-capable" content="yes"/>
  <meta name="apple-mobile-web-app-status-bar-style"
        content="black-translucent"/>

  <meta name="viewport"
        content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <link rel="stylesheet" href="<?=$path?>bower_components/reveal.js/css/reveal.min.css">
  <link rel="stylesheet" href="<?=$path?>bower_components/reveal.js/css/theme/default.css"
        id="theme">

  <!-- For syntax highlighting -->
  <link rel="stylesheet" href="<?=$path?>bower_components/reveal.js/lib/css/zenburn.css">

  <!-- If the query includes 'print-pdf', include the PDF print sheet -->
  <script>
    if (window.location.search.match(/print-pdf/gi)) {
      var link = document.createElement('link');
      link.rel = 'stylesheet';
      link.type = 'text/css';
      link.href = 'bower_components/reveal.js/css/print/pdf.css';
      document.getElementsByTagName('head')[0].appendChild(link);
    }
  </script>

  <!--[if lt IE 9]>
  <script src="bower_components/reveal.js/lib/js/html5shiv.js"></script>
  <![endif]-->
</head>

<body>

<div class="reveal">

  <!-- Any section element inside of this container is displayed as a slide -->
  <div class="slides">
    <section>
      <h1>YouTrack</h1>

      <h3>The art of bug tracking</h3>
      <iframe width="560" height="315" src="//www.youtube.com/embed/4CCiGZyEIGM" frameborder="0" allowfullscreen></iframe>
    </section>

    <section>
      <section>
        <h2>Overview</h2>
        <img src="<?=$path?>images/overview/overview.png"/>
      </section>

      <section>
        <h2>Create issue button</h2>
        <p><img src="<?=$path?>images/overview/create_issue.png"/></p>
        <ul>
          <li>Does pretty much what you expect</li>
          <li>Options to create pre-linked issues (not worth remembering)</li>
          <li><strong>Shortcut:</strong> <code>Ctrl+n</code></li>
        </ul>
      </section>

      <section>
        <h2>Search Bar</h2>
        <p><img src="<?=$path?>images/overview/search_bar.png"/></p>
        <ul>
          <li>Most important navigation tool</li>
          <li>&hellip; don't worry, autocompletion will help.</li>
          <li>Accepts fulltext search, filters and sorts:
            <ul>
              <li><code>Logo assigned to: pmelab order by: created</code></li>
              <li><code>State: Investigation Manager: igor</code></li>
              <li><code>State: -Deployed</code></li>
            </ul>
          </li>
          <li><strong>Shortcut:</strong> <code>Esc</code></li>
        </ul>
      </section>

      <section>
        <h2>Search Context</h2>
        <p><img src="<?=$path?>images/overview/context.png" /></p>
        <ul>
          <li><em>Combine search with other search</em></li>
          <li>Not too useful most of the time</li>
          <li><strong>Common error:</strong> If all issues are gone, perhaps you've set a context.</li>
        </ul>
      </section>

      <section>
        <h2>Search & Context Video</h2>
        <iframe width="560" height="315" src="//www.youtube.com/embed/fzkKcG7KIhI" frameborder="0" allowfullscreen></iframe>
      </section>

      <section>
        <h2>Saved searches</h2>
        <p><img src="<?=$path?>images/overview/saved_searches.png"/></p>
        <ul>
          <li>Save searches for reuse (link under search bar)</li>
          <li>Simply click, or reuse in Search:
            <ul>
             <li><code>#{My Tasks} State:Open</code></li>
            </ul>
          </li>
          <li><em>Autocompletion helps again!</em></li>
        </ul>
      </section>

      <section>
        <h2>Issue List</h2>
        <p><img src="<?=$path?>images/overview/issue_list.png"/></p>
        <ul>
          <li>Select one, some or all issues in the list (<code>Space</code>)</li>
          <li>Navigate using up/down arrows</li>
          <li>Hit <code>Enter</code> to view, <code>F2</code> to edit.</li>
          <li>Various action buttons on top. <em>Don't use them, because &hellip;</em></li>
        </ul>
      </section>

      <section>
        <h2>Command Window</h2>
        <h4>The allmighty <code>Ctrl+Option+j</code></h4>
        <p><img src="<?=$path?>images/overview/command_window.png"/></p>
        <ul>
          <li>Can perform nearly everything (except create/edit)</li>
          <li>Single issue or batch processing.</li>
          <li>Autocompletion all over again.</li>
        </ul>
      </section>

      <section>
        <h2>Video: Command Window</h2>
        <iframe width="560" height="315"
                src="//www.youtube.com/embed/jhSTP_M7_Ns?list=PLQ176FUIyIUbGE728KezWz1J15fHW0S_m"
                frameborder="0" allowfullscreen></iframe>
      </section>

    </section>

    <section>

      <section>
        <h1>I have issues</h1>
        <h3><em>Tell us about it &hellip;</em></h3>
      </section>

      <section>
        <h2>Create a new issue</h2>
        <p>Hit <code>Ctrl+n</code> to bring up a new issue form</p>
        <p><img src="<?=$path?>images/create/create.png"/></p>
        <p>Make sure the correct project is selected.</p>
      </section>

      <section>
        <h2>Summary</h2>
        <h4>Concise description</h4>
        <ul>
          <li><strong>Not that great:</strong>
            <ul>
              <li><em>Image not working</em></li>
            </ul>
          </li>
          <li><strong>Better:</strong>
            <ul>
              <li><em>Error message when uploading image</em></li>
              <li><em>Uploaded logo image not displayed</em></li>
            </ul>
          </li>
        </ul>
      </section>

      <section>
        <h2>Description</h2>
        <h4>Decide between:</h4>
        <p><img height="200" src="<?=$path?>images/create/bug-vs-feature.gif"/></p>
        <ul>
          <li><strong>Bug:</strong> Something doesn't behave as it should.</li>
          <li><strong>Feature:</strong> I need something to do my job.</li>
        </ul>
      </section>

      <section>
        <h2>Feature</h2>
        <ul>
          <li><strong>Given &hellip;</strong> What are the circumstances?</li>
          <li><strong>&hellip; I want to &hellip;</strong> What do you want to do?</li>
          <li><strong>&hellip; to &hellip;</strong> To achieve which goal?</li>
        </ul>
        <p></p>
        <p><em><strong>Given</strong> I am editing a "Institutions and Structures" Page, <strong>I want to</strong> upload a logo image <strong>to</strong> display it in the sidebar above the navigation.</em></p>
      </section>

      <section>
        <h2>Bug</h2>
        <ul>
          <li><strong>If I &hellip;</strong> What exactly are you trying to do?</li>
          <li><strong>&hellip; I expect &hellip;</strong> What <em>should</em> happen?</li>
          <li><strong>&hellip; but instead &hellip;</strong> What <em>happens instead</em>?</li>
        </ul>
        <p></p>
        <p><em><strong>If I</strong> uploaded an image to the logo field and saved the page, <strong>I expect</strong> the image to appear above the navigation, <strong>but instead</strong> it is displayed below.</em></p>
      </section>

      <section>
        <h2>Description Formatting</h2>
        <p>Wiki Markup (see link above Summary)</p>
        <pre><code class="hljs">
==This is a headline ==

-list item 1
-list item 2
-list item 3

...
          </code></pre>
      </section>

      <section>
        <h2>Similar Issues</h2>
        <p><img src="<?=$path?>images/create/similar.png"/></p>
        <ul>
          <li>Not edited by you.</li>
          <li>Just displays issues that match your text.</li>
          <li>Possible reasons why your issue will be ignored.</li>
        </ul>
      </section>


      <section>
        <h2>Attachments</h2>
        <p><img src="<?=$path?>images/create/attachments.png"/></p>
        <ul>
          <li>Attach anything that helps to reproduce the problem.</li>
          <li>Drag & Drop works more or less modern browsers.</li>
        </ul>
      </section>

      <section>
        <h2>Done!</h2>
        <p>Hit <em>Create Issue</em> or <code>Ctrl+Enter</code></p>
      </section>

      <section>
        <h2>One more thing &hellip;</h2>
        <p><img src="<?=$path?>images/create/bookmarklet.png"/></p>
        <h4>And report issues without even using YouTrack!</h4>
      </section>

      <section>
        <h2>Attach Screenshots</h2>
        <p><img src="<?=$path?>images/create/bookmarklet_active.png"/></p>
        <ul>
          <li>Takes screenshot</li>
          <li>Crop and annotate</li>
          <li>Upload - Done!</li>
        </ul>
      </section>

      <section>
        <h2>Screenshot tool</h2>
        <p>Jump to 2:43.</p>
        <iframe width="854" height="510" src="//www.youtube.com/embed/Pp13TkwwANo#t=163" frameborder="0" allowfullscreen></iframe>
      </section>

    </section>

    <section>
      <section>
        <h1>Whats next?</h1>
        <h4>Ticket processing states</h4>
      </section>

      <section>
        <h2>Submitted</h2>
        <ul>
          <li>Initial state</li>
          <li>The ticket has just been submitted.</li>
          <li>Waiting for the team to investigate it.</li>
        </ul>
      </section>

      <section>
        <h2>Duplicate</h2>
        <ul>
          <li>There is another issue with the same specification.</li>
          <li>The other issue is linked under <em>duplicates</em>.</li>
          <li>This issue will not be processed any further.</li>
        </ul>
      </section>

      <section>
        <h2>Obsolete</h2>
        <ul>
          <li>This ticket will not be fixed.</li>
          <li>The reason is explained in a comment.</li>
          <li>Possible reasons:
            <ul>
              <li>Technically not possible</li>
              <li>Possible but too expensive/time consuming</li>
              <li>Magically fixed itself (it happens)</li>
            </ul>
          </li>
        </ul>
      </section>

      <section>
        <h2>Investigation</h2>
        <ul>
          <li>The development team is investigating the issue.</li>
          <li>They may come back to you for further information.</li>
          <li>May still become obsolete or duplicate!</li>
        </ul>
      </section>

      <section>
        <h2>Estimated</h2>
        <ul>
          <li>The effort to resolve this issue has been estimated.</li>
          <li>At least it's not a duplicate.</li>
          <li>May still become obsolete (too expensive)!</li>
        </ul>
      </section>

      <section>
        <h2>Open</h2>
        <ul>
          <li>The issue is queued to be fixed.</li>
          <li>May move back to <em>Investigation</em> if problems occur.</li>
        </ul>
      </section>

      <section>
        <h2>Fixed</h2>
        <ul>
          <li>The developer implemented the solution.</li>
          <li>Ready for testing.</li>
          <li><em>Soon:</em> The solution is testable on a temporary test site.</li>
        </ul>
      </section>

      <section>
        <h2>Verified</h2>
        <ul>
          <li>Isolated testing of this issue was successful.</li>
          <li>The solution <strong>will be</strong> applied to the staging site.</li>
          <li>Integration testing to eliminate side effects.</li>
          <li><strong>NO WAY BACK:</strong>
            <ul>
              <li>The ticket can not be reopened.</li>
              <li>Further problems have to be filed as new bugs.</li>
            </ul>
        </ul>
      </section>

      <section>
        <h2>Staged</h2>
        <p>The solution <strong>has been</strong> applied to the staging site.</p>
        <p>Integration testing.</p>
      </section>

      <section>
        <h2>Deployed</h2>
        <p>The solution has been applied to the <strong>live site</strong>.</p>
        <p>Happens after a release. Most time multiple tickets at once.</p>
      </section>

    </section>


    <section>
      <section>
        <h1>Priorities</h1>
        <h4>Not a matter of gut feelings</h4>
      </section>

      <section>
        <h2>Show-stopper</h2>
        <ul>
          <li>The system is not able to fulfill it's purpose.</li>
          <li>Our case: <em>Deliver information</em></li>
          <li>Examples:
            <ul>
              <li>A certain page is plain white because of an error.</li>
              <li>Editors can't input information because of a malfunction.</li>
              <li>Information displayed is wrong (e.g. calculated date).</li>
            </ul>
          </li>
        </ul>
      </section>

      <section>
        <h2>Critical</h2>
        <ul>
          <li>The system is working, but accessibility is limited.</li>
          <li>I'm not talking about screenreaders.</li>
          <li>Examples:
            <ul>
              <li>A story is listed, but not displayed in the "Recent Stories" block.</li>
              <li>Editors have to write HTML because WYSIWYG does not work correctly.</li>
              <li>Performance issues - Response time is too high.</li>
            </ul>
          </li>
        </ul>
      </section>

      <section>
        <h2>Normal</h2>
        <ul>
          <li>The system is working, but accessibility is limited for certain users.</li>
          <li>Examples:
            <ul>
              <li>Slider doesn't work for Internet Explorer 8 and lower.</li>
              <li>Page <em>looks weird</em> when displayed in right-to-left languages.</li>
              <li>Print display doesn't behave consistently across browsers.</li>
            </ul>
          </li>
        </ul>
      </section>

      <section>
        <h2>Minor</h2>
        <ul>
          <li>Users probably don't even recognize there is a problem.</li>
          <li>Enhancements that help save time and money.</li>
          <li>Examples:
            <ul>
              <li>Better image chooser for editors.</li>
              <li>Refactoring of a component that may cause problems on the long run.</li>
            </ul>
          </li>
        </ul>
      </section>

      <section>
        <h2>On hold</h2>
        <ul>
          <li>Improvement that may be relevant in the future, but not now.</li>
          <li>Example:
            <ul>
              <li>Migrate to Drupal 8</li>
              <li>Try elastic search instead of Solr.</li>
            </ul>
          </li>
        </ul>
      </section>

    </section>

    <section>
      <section>
        <h1>The Future</h1>
        <h4>YouTrack Harmony - Fall 2014</h4>
      </section>
      <section>
        <h2>Dashboard</h2>
        <p><img src="<?=$path?>images/dashboard.png"/></p>
      </section>
      <section>
        <h2>Alternative UI</h2>
        <p><img src="<?=$path?>images/geek_ui.png"/></p>
      </section>
      <section>
        <h2>And much more &hellip;</h2>
        <ul>
          <li>AND-Queries in search box</li>
          <li>Multiple issue drafts</li>
          <li>More detailed reports</li>
          <li>&hellip;</li>
        </ul>
      </section>
    </section>
  </div>

</div>

<script src="<?=$path?>bower_components/reveal.js/lib/js/head.min.js"></script>
<script src="<?=$path?>bower_components/reveal.js/js/reveal.min.js"></script>

<script>

  // Full list of configuration options available here:
  // https://github.com/hakimel/reveal.js#configuration
  Reveal.initialize({
    controls: true,
    progress: true,
    history: true,
    center: true,

    theme: Reveal.getQueryHash().theme, // available themes are in /css/theme
    transition: Reveal.getQueryHash().transition || 'default', // default/cube/page/concave/zoom/linear/fade/none

    // Parallax scrolling
    // parallaxBackgroundImage: 'https://s3.amazonaws.com/hakim-static/reveal-js/reveal-parallax-1.jpg',
    // parallaxBackgroundSize: '2100px 900px',

    // Optional libraries used to extend on reveal.js
    dependencies: [
      {
        src: '<?=$path?>bower_components/reveal.js/lib/js/classList.js',
        condition: function () {
          return !document.body.classList;
        }
      },
      {
        src: '<?=$path?>bower_components/reveal.js/plugin/markdown/marked.js',
        condition: function () {
          return !!document.querySelector('[data-markdown]');
        }
      },
      {
        src: '<?=$path?>bower_components/reveal.js/plugin/markdown/markdown.js',
        condition: function () {
          return !!document.querySelector('[data-markdown]');
        }
      },
      {
        src: '<?=$path?>bower_components/reveal.js/plugin/highlight/highlight.js',
        async: true,
        callback: function () {
          hljs.initHighlightingOnLoad();
        }
      },
      {
        src: '<?=$path?>bower_components/reveal.js/plugin/zoom-js/zoom.js',
        async: true,
        condition: function () {
          return !!document.body.classList;
        }
      },
      {
        src: '<?=$path?>bower_components/reveal.js/plugin/notes/notes.js',
        async: true,
        condition: function () {
          return !!document.body.classList;
        }
      }
    ]
  });

</script>

</body>
</html>
